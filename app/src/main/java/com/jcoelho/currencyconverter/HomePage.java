package com.jcoelho.currencyconverter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Optional;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import pl.utkala.searchablespinner.SearchableSpinner;

public class HomePage extends NavigationPane {

    private Handler mDelayHandler = null;
    private Long splashDelay = 1000L;
    public static HomeFeed homeFeed;
    private SearchableSpinner fromCurr = findViewById(R.id.fromCurr);
    private SearchableSpinner toCurr = findViewById(R.id.toCurr);


    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            ArrayAdapter arrayAdapter = new ArrayAdapter(HomePage.this, android.R.layout.simple_spinner_item, homeFeed.getRates().keySet().toArray());
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            fromCurr.setAdapter(arrayAdapter);
            fromCurr.setSelection(0, true);
            fromCurr.setGravity(Gravity.CENTER);

            toCurr.setAdapter(arrayAdapter);
            toCurr.setSelection(0, true);
            toCurr.setGravity(Gravity.CENTER);


        }
    };

    public void fetchJson() {
        System.out.println("Attempting");

        String url = "http://data.fixer.io/api/latest?access_key=6ea1f933eac1b688ac6c36ff708c6324";

        Request request = new Request.Builder().url(url).build();
        OkHttpClient client = new OkHttpClient();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("Failed to execute request");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String body = response.body().string();
                Gson gson = new GsonBuilder().create();
                homeFeed = gson.fromJson(body, HomeFeed.class);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        onCreateDrawer(mDrawerLayout);

        Button convertBtn = findViewById(R.id.convertBtn);
        TextView enterAmount = findViewById(R.id.enterAmt);

        fetchJson();

        convertBtn.setOnClickListener((v) -> {
                    Intent resultIntent = new Intent(this, ResultPage.class);
                    if (enterAmount.getText() != null) {
                        String amount = enterAmount.getText().toString();
                        Float finalValue = conversion(fromCurr.getSelectedItem().toString(), toCurr.getSelectedItem().toString(), amount);
                        resultIntent.putExtra("key", finalValue.toString());
                        startActivity(resultIntent);
                    }
                }
        );

        mDelayHandler = new Handler();
        if (mDelayHandler != null) {
            mDelayHandler.postDelayed(mRunnable, splashDelay);
        }
    }

    private Float conversion(String from, String to, String amount) {

        String fromCurrRate = homeFeed.getRates().get(from).toString();
        String toCurrRate = homeFeed.getRates().get(to).toString();

        return ((Float.parseFloat(amount) * Float.parseFloat(toCurrRate)) / Float.parseFloat(fromCurrRate));
    }
}