package com.jcoelho.currencyconverter;

import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

public class NavigationPane extends AppCompatActivity {

    private ActionBarDrawerToggle mToggle;
    private String userID = "joao.coelho@gmail.com";
    private String username = "João Coelho";

    public void onCreateDrawer(DrawerLayout mDrawerLayout) {

        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView emailId = headerView.findViewById(R.id.email_ID);
        TextView nameId = headerView.findViewById(R.id.name_ID);

        emailId.setText(userID);
        nameId.setText(username);

        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.isChecked();
                mDrawerLayout.closeDrawers();

                switch (item.getItemId()) {
                    case R.id.home: {
                        Intent intent = new Intent(NavigationPane.this, HomePage.class);
                        startActivity(intent);
                    }
                    case R.id.market_status: {
                        Intent intent = new Intent(NavigationPane.this, MarketStatusPage.class);
                        startActivity(intent);
                    }
                    case R.id.help: {
                        Intent intent = new Intent(NavigationPane.this, HelpPage.class);
                        startActivity(intent);
                    }
                    case R.id.contact: {
                        Toast.makeText(NavigationPane.this, "Visit: www.loonycorn.com", Toast.LENGTH_LONG).show();
                    }
                }
                return true;
            }
        });


        /*navigationView.setNavigationItemSelectedListener((item -> {
            item.isChecked();
            mDrawerLayout.closeDrawers();

            switch (item.getItemId()) {
                case R.id.home: {
                    Intent intent = new Intent(NavigationPane.this, HomePage.class);
                    startActivity(intent);
                }
                case R.id.market_status: {
                    Intent intent = new Intent(NavigationPane.this, MarketStatusPage.class);
                    startActivity(intent);
                }
                case R.id.help: {
                    Intent intent = new Intent(NavigationPane.this, HelpPage.class);
                    startActivity(intent);
                }
                case R.id.contact: {
                    Toast.makeText(NavigationPane.this, "Visit: www.loonycorn.com", Toast.LENGTH_LONG).show();
                }
            }
            return true;
        }));*/

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
