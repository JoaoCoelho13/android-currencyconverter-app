package com.jcoelho.currencyconverter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.widget.TextView;

public class HelpPage extends NavigationPane {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_page);

        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        onCreateDrawer(mDrawerLayout);

        TextView help_page_ans_numCurr = findViewById(R.id.help_page_ans_numcurr);
        TextView help_page_ans_baseCurr = findViewById(R.id.help_page_ans_basecurr);
        help_page_ans_numCurr.setText(HomePage.homeFeed.getRates().keySet().size());
        help_page_ans_baseCurr.setText(HomePage.homeFeed.getBase());
    }
}