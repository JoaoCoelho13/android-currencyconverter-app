package com.jcoelho.currencyconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreen extends AppCompatActivity {

    private Handler delayHandler = null;
    private Long splashDelay = 3000L;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Intent homeIntent = new Intent(SplashScreen.this, HomePage.class);
            finish();
            startActivity(homeIntent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        delayHandler = new Handler();
        if (delayHandler != null) {
            delayHandler.postDelayed(runnable, splashDelay);
        }
    }

    @Override
    public void onDestroy() {
        if (delayHandler!=null) {
            delayHandler.removeCallbacks(runnable);
        }
        super.onDestroy();
    }
}