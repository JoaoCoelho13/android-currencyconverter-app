package com.jcoelho.currencyconverter;


import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.drawerlayout.widget.DrawerLayout;

import com.google.gson.JsonObject;

import java.util.ArrayList;

public class MarketStatusPage extends NavigationPane {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_status_page);

        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        onCreateDrawer(mDrawerLayout);

        TextView baseCurrency = findViewById(R.id.base_currency);
        baseCurrency.setText(HomePage.homeFeed.getBase());

        ArrayList<String> array = new ArrayList<>();

        for(String string : HomePage.homeFeed.getRates().keySet()) {
            array.add(string + " : " + HomePage.homeFeed.getRates().get(string));
        }

        ListView mListView = findViewById(R.id.userList);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, array);
        mListView.setAdapter(arrayAdapter);

    }
}