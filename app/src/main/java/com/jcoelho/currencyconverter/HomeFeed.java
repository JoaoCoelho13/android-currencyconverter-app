package com.jcoelho.currencyconverter;

import com.google.gson.JsonObject;

public class HomeFeed {
    private JsonObject rates;
    private String base;

    public JsonObject getRates() {
        return rates;
    }

    public void setRates(JsonObject rates) {
        this.rates = rates;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }
}
