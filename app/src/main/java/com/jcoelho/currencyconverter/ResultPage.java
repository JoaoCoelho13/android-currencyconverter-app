package com.jcoelho.currencyconverter;


import android.os.Bundle;
import android.widget.TextView;

import androidx.drawerlayout.widget.DrawerLayout;

public class ResultPage extends NavigationPane {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_page);

        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        onCreateDrawer(mDrawerLayout);

        String result = getIntent().getStringExtra("key");
        TextView resultPage = findViewById(R.id.result_page);
        resultPage.setText(result);
    }
}